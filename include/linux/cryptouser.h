/*
 * Crypto user configuration API.
 *
 * Copyright (C) 2011 secunet Security Networks AG
 * Copyright (C) 2011 Steffen Klassert <steffen.klassert@secunet.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <linux/types.h>
#include <libnetlink.h>

#define CRYPTO_MAX_ALG_NAME 64
#define NLMSG_BUF_SIZE 4096

/*  
 * Algorithm masks and types. 
 */ 
#define CRYPTO_ALG_TYPE_MASK            0x0000000f
#define CRYPTO_ALG_TYPE_CIPHER          0x00000001
#define CRYPTO_ALG_TYPE_COMPRESS        0x00000002
#define CRYPTO_ALG_TYPE_AEAD            0x00000003
#define CRYPTO_ALG_TYPE_BLKCIPHER       0x00000004
#define CRYPTO_ALG_TYPE_ABLKCIPHER      0x00000005
#define CRYPTO_ALG_TYPE_GIVCIPHER       0x00000006
#define CRYPTO_ALG_TYPE_DIGEST          0x00000008
#define CRYPTO_ALG_TYPE_HASH            0x00000008
#define CRYPTO_ALG_TYPE_SHASH           0x00000009
#define CRYPTO_ALG_TYPE_AHASH           0x0000000a
#define CRYPTO_ALG_TYPE_RNG             0x0000000c
#define CRYPTO_ALG_TYPE_PCOMPRESS       0x0000000f

#define CRYPTO_ALG_TYPE_HASH_MASK       0x0000000e
#define CRYPTO_ALG_TYPE_AHASH_MASK      0x0000000c
#define CRYPTO_ALG_TYPE_BLKCIPHER_MASK  0x0000000c

/* Netlink configuration messages.  */
enum {
	CRYPTO_MSG_BASE = 0x10,
	CRYPTO_MSG_NEWALG = 0x10,
	CRYPTO_MSG_DELALG,
	CRYPTO_MSG_GETALG,
	__CRYPTO_MSG_MAX
};
#define CRYPTO_MSG_MAX (__CRYPTO_MSG_MAX - 1)
#define CRYPTO_NR_MSGTYPES (CRYPTO_MSG_MAX + 1 - CRYPTO_MSG_BASE)

/* Netlink message attributes.  */
enum crypto_attr_type_t {
	CRYPTOCFGA_UNSPEC,
	CRYPTOCFGA_PRIORITY_VAL,	/* __u32 */
	CRYPTOCFGA_REPORT_LARVAL,       /* struct crypto_report_larval */
	CRYPTOCFGA_REPORT_SHASH,        /* struct crypto_report_shash */
	CRYPTOCFGA_REPORT_AHASH,	/* struct crypto_report_ahash */
	CRYPTOCFGA_REPORT_BLKCIPHER,	/* struct crypto_report_blkcipher */
	CRYPTOCFGA_REPORT_ABLKCIPHER,	/* struct crypto_report_ablkcipher */
	CRYPTOCFGA_REPORT_GIVCIPHER,	/* struct crypto_report_ablkcipher */
	CRYPTOCFGA_REPORT_AEAD,		/* struct crypto_report_aead */
	CRYPTOCFGA_REPORT_NIVAEAD,	/* struct crypto_report_aead */
	CRYPTOCFGA_REPORT_PCOMPRESS,	/* struct crypto_report_comp */
	CRYPTOCFGA_REPORT_RNG,		/* struct crypto_report_rng */
	CRYPTOCFGA_REPORT_CIPHER,	/* struct crypto_report_cipher */
	CRYPTOCFGA_REPORT_COMPRESS,	/* struct crypto_report_comp */
	__CRYPTOCFGA_MAX

#define CRYPTOCFGA_MAX (__CRYPTOCFGA_MAX - 1)
};

struct crypto_user_alg {
	char cru_name[CRYPTO_MAX_ALG_NAME];
	char cru_driver_name[CRYPTO_MAX_ALG_NAME];
	__u32 type;
	__u32 mask;
};

#define CRYPTO_MAX_NAME CRYPTO_MAX_ALG_NAME

struct crypto_report_base {
	char name[CRYPTO_MAX_ALG_NAME];
	char driver_name[CRYPTO_MAX_ALG_NAME];
	char module_name[CRYPTO_MAX_NAME];
	char selftest[CRYPTO_MAX_NAME];
	int priority;
	int refcnt;
};

struct crypto_report_larval {
	char type[CRYPTO_MAX_NAME];
	__u32 flags;
};

struct crypto_report_shash {
	char type[CRYPTO_MAX_NAME];
	unsigned int blocksize;
	unsigned int digestsize;
};

struct crypto_report_ahash {
	char type[CRYPTO_MAX_NAME];
	char async[CRYPTO_MAX_NAME];
	unsigned int blocksize;
	unsigned int digestsize;
};

struct crypto_report_cipher {
	char type[CRYPTO_MAX_NAME];
	unsigned int blocksize;
	unsigned int min_keysize;
	unsigned int max_keysize;
};

struct crypto_report_blkcipher {
	char type[CRYPTO_MAX_NAME];
	char geniv[CRYPTO_MAX_NAME];
	unsigned int blocksize;
	unsigned int min_keysize;
	unsigned int max_keysize;
	unsigned int ivsize;
};

struct crypto_report_ablkcipher {
	char type[CRYPTO_MAX_NAME];
	char async[CRYPTO_MAX_NAME];
	char geniv[CRYPTO_MAX_NAME];
	unsigned int blocksize;
	unsigned int min_keysize;
	unsigned int max_keysize;
	unsigned int ivsize;
};

struct crypto_report_aead {
	char type[CRYPTO_MAX_NAME];
	char async[CRYPTO_MAX_NAME];
	char geniv[CRYPTO_MAX_NAME];
	unsigned int blocksize;
	unsigned int maxauthsize;
	unsigned int ivsize;
};

struct crypto_report_comp {
	char type[CRYPTO_MAX_NAME];
};

struct crypto_report_rng {
	char type[CRYPTO_MAX_NAME];
	unsigned int seedsize;
};

#define CR_RTA(x)  ((struct rtattr*)(((char*)(x)) + NLMSG_ALIGN(sizeof(struct crypto_report_base))))

